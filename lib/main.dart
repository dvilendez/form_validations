import 'package:flutter/material.dart';

import 'package:form_validations/src/blocs/provider.dart';

import 'package:form_validations/src/pages/home_page.dart';
import 'package:form_validations/src/pages/login_page.dart';
import 'package:form_validations/src/pages/product_page.dart';
import 'package:form_validations/src/pages/register_page.dart';
import 'package:form_validations/src/shared_prefs/user_preferences.dart';
 
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new UserPreferences();
  await prefs.initPrefs();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _prefs = new UserPreferences();

    print(_prefs.token);

    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        // initialRoute: (_prefs.token != null) ? 'home' : 'login',
        initialRoute: 'login',
        routes: {
          'login'    : ( BuildContext context) => LoginPage(),
          'home'     : ( BuildContext context) => HomePage(),
          'product'  : ( BuildContext context) => ProductPage(),
          'register' : ( BuildContext context) => RegisterPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.deepPurple
        ),
      )
    );
  }
}