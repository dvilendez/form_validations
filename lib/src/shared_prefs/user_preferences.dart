import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {

  static final UserPreferences _instance = new UserPreferences._internal();

  factory UserPreferences () {
    return _instance;
  }

  UserPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  // GET and SET gender
  get token {
    return _prefs.getString('token') ?? 1;
  }
  
  set token ( String value ) {
    _prefs.setString('token', value);
  }

  // GET and SET lastPage
  get lastPage {
    return _prefs.getString('lastPage') ?? 'home';
  }
  
  set lastPage ( String value ) {
    _prefs.setString('lastPage', value);
  }


}