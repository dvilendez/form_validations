import 'dart:convert';
import 'package:form_validations/src/shared_prefs/user_preferences.dart';
import 'package:http/http.dart' as http;

class UserProvider {

  final String _firebaseToken = 'AIzaSyBlmKcOhsj3IDj48kOzr16su6-q69Fng9U';
  final _prefs = new UserPreferences();

  login ( String email, String password ) async {
    final authData = {
      'email'             : email,
      'password'          : password,
      'returnSecureToken' : true
    };

    final resp = await http.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$_firebaseToken',
      body: json.encode( authData )
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print('=============');
    print(decodedResp);
    print('=============');

    if ( decodedResp.containsKey( 'idToken' ) ) {
      _prefs.token = decodedResp['idToken'];
      return { 'ok': true, 'token': decodedResp['idToken'] };
    } else {
      return { 'ok': false, 'message': decodedResp['error']['message'] };
    }
  }

  Future<Map<String, dynamic>> newUser ( String email, String password ) async {
    final authData = {
      'email'             : email,
      'password'          : password,
      'returnSecureToken' : true
    };

    final resp = await http.post(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$_firebaseToken',
      // 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$_firebaseToken',
      body: json.encode( authData )
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print('=============');
    print(decodedResp);
    print('=============');

    if ( decodedResp.containsKey( 'idToken' ) ) {
      _prefs.token = decodedResp['idToken'];
      return { 'ok': true, 'token': decodedResp['idToken'] };
    } else {
      return { 'ok': false, 'message': decodedResp['error']['message'] };
    }
  }


}