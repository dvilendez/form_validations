import 'package:flutter/material.dart';

import 'package:form_validations/src/blocs/provider.dart';
import 'package:form_validations/src/models/product_model.dart';
import 'package:form_validations/src/providers/products_provider.dart';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final productsBloc = Provider.productsBloc(context);
    productsBloc.loadProducts();

    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: _createList(productsBloc),
      floatingActionButton: _createButton(context),
    );
  }

  Widget _createList (ProductsBloc productsBloc) {

    return StreamBuilder(
      stream: productsBloc.productsStream ,
      builder: (BuildContext context, AsyncSnapshot<List<ProductModel>> snapshot){
        if ( snapshot.hasData ) {
          final products = snapshot.data;
          return ListView.builder(
            itemCount: products.length,
            itemBuilder: (context, i) => _createItem(context, productsBloc, products[i]),
          );
        } else {
          return Center( child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _createItem (BuildContext context, ProductsBloc productsBloc, ProductModel product) {
    return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red
      ),
      onDismissed: ( direction ) => productsBloc.deleteProduct(product.id),
      child: Card(
        child: Column(
          children: <Widget>[
            ( product.urlImage == null )
              ? Image(image: AssetImage('assets/no-image.png'))
              : FadeInImage(
                image: NetworkImage( product.urlImage ),
                placeholder: AssetImage('assets/jar-loading.gif'),
                height: 300.0,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
            ListTile(
              title: Text('${ product.title } - ${ product.price }'),
              subtitle: Text( product.id ),
              onTap: () => Navigator.pushNamed(context, 'product', arguments: product),
            )
          ],
        ),
      ),
    );

    
  }

  Widget _createButton (BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: Colors.deepPurple,
      onPressed: () => Navigator.pushNamed(context, 'product'),
    );
  }
}