import 'dart:io';

import 'package:flutter/material.dart';
import 'package:form_validations/src/blocs/provider.dart';

import 'package:form_validations/src/utils/utils.dart' as utils;

import 'package:form_validations/src/models/product_model.dart';
import 'package:image_picker/image_picker.dart';

class ProductPage extends StatefulWidget {

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  ProductsBloc productsBloc;
  ProductModel product = new ProductModel();
  bool _saving = false;
  File photo;


  @override
  Widget build(BuildContext context) {

    productsBloc = Provider.productsBloc(context);

    final ProductModel prodData = ModalRoute.of(context).settings.arguments;
    if ( prodData != null ) {
      product = prodData;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Product'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual),
            onPressed: _selectImage,
          ),
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _takeImage,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _showImage(),
                _createName(),
                _createPrice(),
                _createAvailable(),
                _createButton()
              ],
            ),
          )
        ),
      ),
    );
  }

  Widget _createName () {
    return TextFormField(
      initialValue: product.title,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: 'Producto'
      ),
      onSaved: ( value ) => product.title = value,
      validator: ( value ) {
        if (value.length < 3) {
          return 'Ingrese el nombre del producto';
        } else {
          return null;
        }
      },
    );
  }

  Widget _createPrice () {
    return TextFormField(
      initialValue: product.price.toString(),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        labelText: 'Precio'
      ),
      onSaved: ( value ) => product.price = double.parse(value),
      validator: ( value ) {
        if ( utils.isNumeric(value) ) {
          return null;
        } else {
          return 'Ingrese solo números';
        }
      },
    );
  }

  Widget _createAvailable () {
    return SwitchListTile(
      value: product.available,
      title: Text('Disponible'),
      activeColor: Colors.deepPurple,
      onChanged: ( value ) => setState((){
        product.available = value;
      }),
    );
  }

  Widget _createButton () {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0)
      ),
      color: Colors.deepPurple,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_saving) ? null : _submit,
    );
  }

  void _submit () async {
    
    if ( !formKey.currentState.validate() ) return;

    formKey.currentState.save();
    
    setState(() { _saving = true; });

    if ( photo != null ) {
      product.urlImage = await productsBloc.uploadImage(photo);
    }

    if ( product.id != null ) {
      productsBloc.modifyProduct(product);
    } else {
      productsBloc.createProduct(product);
    }
    // setState(() { _saving = false; });
    showSnackbar('Registro guardado');

    Navigator.pop(context);
  }

  void showSnackbar (String message) {
    final snackbar = SnackBar(
      content: Text(message),
      duration: Duration(milliseconds: 1500),
    );

    scaffoldKey.currentState.showSnackBar(snackbar);

  }

  _showImage () {
    if (product.urlImage != null) {
      return FadeInImage(
        image: NetworkImage( product.urlImage ),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        width: double.infinity,
        fit: BoxFit.cover,
      );
    } else {
      return Image(
        image: (photo?.path != null) ? FileImage(photo) : AssetImage('assets/no-image.png'),
        height: 300.0,
        fit: BoxFit.cover,
      );
    }
  }


  _selectImage () async {
    _processImage(ImageSource.gallery);
  }

  _takeImage () async {
    _processImage(ImageSource.camera);
  }

  _processImage(ImageSource source) async {
    photo = await ImagePicker.pickImage(
      source: source
    );

    if ( photo != null ) {
      product.urlImage = null;
    }

    setState(() {});
  }
}